package schedule;

/**
 * 0 - NEW
 * 1 - READY
 * 2 - RUNNING 
 * 3 - TERMINATED
 * Enum used for process state
 */
public enum ProcessState {
    NEW, 
    READY, 
    RUNNING, 
    TERMINATED
}
